﻿using FluentMigrator;
using FluentMigrator.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using FluentMigrator.Runner.Extensions;

namespace CorpoEAlma.Db.Migrations
{
  [Migration(20180722385401)]
  public class _20180722385401_CriarTabelaDeUsuario : Migration
  {
    internal static string TabelaDeUsuario = "Usuario";

    public override void Up()
    {
      Create.Table(TabelaDeUsuario)
        .WithColumn("Id").AsInt32().Identity().PrimaryKey()
        .WithColumn("Nome").AsString(40).NotNullable().WithDefaultValue(string.Empty)
        .WithColumn("Sobrenome").AsString(60).Nullable().WithDefaultValue(string.Empty)
        .WithColumn("Cpf").AsString(14).NotNullable().WithDefaultValue(string.Empty)
        .WithColumn("Telefone").AsString(20).NotNullable().WithDefaultValue(string.Empty)
        .WithColumn("Email").AsString(200).NotNullable().WithDefaultValue(string.Empty)
        .WithColumn("Login").AsString(80).NotNullable().WithDefaultValue(string.Empty)
        .WithColumn("Senha").AsString(200).NotNullable().WithDefaultValue(string.Empty)
        .WithColumn("DicaSenha").AsString(200).NotNullable().WithDefaultValue(string.Empty)
        .WithColumn("CaminhoFoto").AsString(8000).NotNullable().WithDefaultValue(string.Empty)
        .WithColumn("Bloqueado").AsBoolean().Nullable().WithDefaultValue(false)
        .WithColumn("DataDeCadastro").AsDateTimeOffset().NotNullable().WithDefaultValue(DateTimeOffset.Now);

    }

    public override void Down()
    {
      Delete.Table(TabelaDeUsuario);
    }
  }
}
