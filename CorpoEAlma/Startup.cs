﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using CorpoEAlma.Db;
using CorpoEAlma.Models;
using CorpoEAlma.Models.Authenticacao;

namespace CorpoEAlma
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      ConfigureAuthentication(services);

      services.AddMvc();

      var connectionString = Configuration.GetConnectionString("CorpoAlma");

      //#if DEBUG
      //MigrationRunner.Down(connectionString);
      MigrationRunner.Up(connectionString);
      //#endif

      services.AddDbContextPool<DbCorpoEAlmaContext>(
        options => options.UseSqlServer(connectionString));


      var logger = services
          .BuildServiceProvider()
          .GetService<DbCorpoEAlmaContext>();

      logger.TestarTodasTabelas();

    }

    private void ConfigureAuthentication(IServiceCollection services)
    {

      var tokenConfigurations = new TokenConfigurations();

      new ConfigureFromConfigurationOptions<TokenConfigurations>(
              Configuration.GetSection("TokenConfigurations"))
          .Configure(tokenConfigurations);

      services.AddSingleton(tokenConfigurations);

      services.AddAuthentication(authOptions =>
      {
        authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
      }).AddJwtBearer(bearerOptions =>
      {
        var paramsValidation = bearerOptions.TokenValidationParameters;
        paramsValidation.IssuerSigningKey =
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenConfigurations.SymmetricSecurityKey));

        paramsValidation.ValidAudience = tokenConfigurations.Audience;
        paramsValidation.ValidIssuer = tokenConfigurations.Issuer;

        // Valida a assinatura de um token recebido
        paramsValidation.ValidateIssuerSigningKey = true;

        // Verifica se um token recebido ainda é válido
        paramsValidation.ValidateLifetime = true;

        // Tempo de tolerância para a expiração de um token (utilizado
        // caso haja problemas de sincronismo de horário entre diferentes
        // computadores envolvidos no processo de comunicação)
        paramsValidation.ClockSkew = TimeSpan.Zero;

        bearerOptions.Events = new JwtBearerEvents()
        {
          OnAuthenticationFailed = OnAuthenticationFailed,
          OnTokenValidated = OnTokenValidated
        };
      });

      // Ativa o uso do token como forma de autorizar o acesso
      // a recursos deste projeto
      services.AddAuthorization(auth =>
      {
        auth.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
            .RequireAuthenticatedUser()
            .RequireClaim(Usuario.CLAINS_DO_CODIGO_DA_CONTA)
            .Build();

        //auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
        //    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
        //    .RequireAuthenticatedUser().Build());
      });
    }

    public async Task OnTokenValidated(TokenValidatedContext arg)
    {
      Debug.Write(arg);
    }

    static async Task OnAuthenticationFailed(AuthenticationFailedContext context)
    {
      Debug.Write(context);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      loggerFactory.AddConsole(Configuration.GetSection("Logging"));
      loggerFactory.AddDebug();

      app.Use(async (context, next) =>
      {
        await next();

        // If there's no available file and the request doesn't contain an extension, we're probably trying to access a page.
        // Rewrite request to use app root
        if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value) && !context.Request.Path.Value.StartsWith("/api"))
        {
          context.Request.Path = "/index.html";
          context.Response.StatusCode = 200; // Make sure we update the status code, otherwise it returns 404
          await next();
        }
      });

      app.UseDefaultFiles();
      app.UseStaticFiles();

      app.UseMvc();
    }
  }
}
