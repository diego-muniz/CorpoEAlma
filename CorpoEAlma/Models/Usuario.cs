﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace CorpoEAlma.Models
{
    public class Usuario
    {
    internal const string CLAINS_DO_CODIGO_DA_CONTA = "conta";

    public int Id { get; set; }
    public string Nome { get; set; }
    public string Sobrenome { get; set; }
    public string Cpf { get; set; }
    public string Telefone { get; set; }
    public string Email { get; set; }

    [IgnoreDataMember]
    public string Senha { get; set; }
    public string Login { get; set; }
    public string DicaSenha { get; set; }

    public string CaminhoFoto { get; set; }
    public bool Bloqueado { get; set; }

    public DateTimeOffset DataDeCadastro { get; set; }
  }
}
