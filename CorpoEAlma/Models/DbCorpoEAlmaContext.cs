﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorpoEAlma.Models
{
  public class DbCorpoEAlmaContext : DbContext
  {
    public DbCorpoEAlmaContext(DbContextOptions dbContextOptions)
      :base(dbContextOptions) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {

      base.OnModelCreating(modelBuilder);
    }

    public void TestarTodasTabelas()
    {
      var temCliente = Cliente.ToList();
    }


    public DbSet<Usuario> Usuario { get; set; }

    public DbSet<Cliente> Cliente { get; set; }

  }
}
